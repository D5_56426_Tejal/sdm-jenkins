const mysql = require("mysql");
const openConnection = () => {
  const connection = mysql.createConnection({
    port: 3306,
    host: "database",
    user: "root",
    password: "root",
    database: "emp_db",
  });
  connection.connect();
  return connection;
};
module.exports = {
  openConnection,
};
