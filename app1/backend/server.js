const express = require("express");
const app = express();
const db = require("./db");
const utils = require("./utils");

app.use(express.json());

app.get("/", (request, response) => {
  console.log("In get");
  const statement = `select * from emp`;
  const connection = db.openConnection();
  connection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});
app.post("/", (request, response) => {
  console.log("In post");
  const {name, salary, age} = request.body;
  const statement = `insert into emp(name,salary,age) values('${name}',${salary},${age})`;
  const connection = db.openConnection();
  connection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});
app.delete("/:id", (request, response) => {
  console.log("In delete");
  const {id} = request.params;
  const statement = `delete from emp where empid=${id}`;
  const connection = db.openConnection();
  connection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});
app.put("/:id", (request, response) => {
  console.log("In put");
  const {salary} = request.body;
  const {id} = request.params;
  const statement = `update emp set salary=${salary} where empid=${id}`;
  const connection = db.openConnection();
  connection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

app.listen(4000, () => {
  console.log("Server started on port 4000");
});
