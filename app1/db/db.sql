ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'root';
FLUSH PRIVILEGES;

CREATE TABLE emp(
    empid integer primary key auto_increment,
    name varchar(100),
    salary float,
    age integer
);